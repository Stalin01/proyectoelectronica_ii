%% Transmisor y Receptor QPSK

modelname = 'commqpsktxrx';
open_system(modelname);
set_param('commqpsktxrx/Model Parameters','ShowRecvMsg','off');
open_system([modelname '/Close Scopes']);
commqpsktxrx([],[],[],'compile');
commqpsktxrx([],[],[],'term');
%%
open_system([modelname '/Transmitter']);
%%
close_system([modelname '/Transmitter']);
open_system([modelname '/Receiver']);

%% Transmitter
close_system([modelname '/Receiver']);
open_system([modelname '/Transmitter/Bit Generation']);
%% AWGN Channel with Frequency Offset and Variable Delay
close_system([modelname '/Transmitter/Bit Generation']);
open_system([modelname '/AWGN Channel with Frequency Offset & Variable Time Delay']);
%% Receiver
close_system([modelname '/AWGN Channel with Frequency Offset & Variable Time Delay']);
open_system([modelname '/Receiver/Coarse Frequency Compensation']);
%%
close_system([modelname '/Receiver/Coarse Frequency Compensation']);
open_system([modelname '/Receiver/Data Decoding']);
%% Results and Displays
close_system([modelname '/Receiver/Data Decoding']);
open_system([modelname '/Receiver/Spectrum']);
set_param([modelname '/Receiver/Spectrum'],'OpenScopeAtSimStart','on');
set_param(modelname, 'StopTime', '100 * qpsktxrx.FrameTime');
sim(modelname);
%%
close_system([modelname '/Receiver/Spectrum']);
set_param([modelname '/Receiver/Spectrum'],'OpenScopeAtSimStart','off');
open_system([modelname '/Receiver/After RC Rx Filter']);
set_param([modelname '/Receiver/After RC Rx Filter'],'OpenScopeAtSimStart','on');
set_param(modelname, 'StopTime', '100 * qpsktxrx.FrameTime');
sim(modelname);
%%
close_system([modelname '/Receiver/After RC Rx Filter']);
set_param([modelname '/Receiver/After RC Rx Filter'],'OpenScopeAtSimStart','off');
open_system([modelname '/Receiver/After Symbol Synchronizer']);
set_param([modelname '/Receiver/After Symbol Synchronizer'],'OpenScopeAtSimStart','on');
set_param(modelname, 'StopTime', '100 * qpsktxrx.FrameTime');
sim(modelname);
%%
close_system([modelname '/Receiver/After Symbol Synchronizer']);
set_param([modelname '/Receiver/After Symbol Synchronizer'],'OpenScopeAtSimStart','off');
open_system([modelname '/Receiver/After Carrier Synchronizer']);
set_param([modelname '/Receiver/After Carrier Synchronizer'],'OpenScopeAtSimStart','on');
set_param(modelname, 'StopTime', '100 * qpsktxrx.FrameTime');
sim(modelname);
%% 
close_system([modelname '/Receiver/After Symbol Synchronizer']);
set_param([modelname '/Receiver/After Symbol Synchronizer'], ...
    'OpenScopeAtSimStart','off');
close_system(modelname, 0);
clear modelname channelSubsys;
