function SimParams = commqpsktxrx_init

%% General simulation parameters
SimParams.ModulationOrder = 4;      
SimParams.Interpolation = 2;        
SimParams.Decimation = 1;           
SimParams.Rsym = 5e4;               
SimParams.Tsym = 1/SimParams.Rsym;  
SimParams.Fs   = SimParams.Rsym * SimParams.Interpolation; 
SimParams.TotalFrame = 1000;        

%% Frame Specifications
% [BarkerCode*2 | 'Hello world 000\n' | 'Hello world 001\n' ...];
SimParams.BarkerCode      = [+1 +1 +1 +1 +1 -1 -1 +1 +1 -1 +1 -1 +1];     
SimParams.BarkerLength    = length(SimParams.BarkerCode);
SimParams.HeaderLength    = SimParams.BarkerLength * 2;                   
SimParams.Message         = 'Hello world';
SimParams.MessageLength   = strlength(SimParams.Message) + 5;                
SimParams.NumberOfMessage = 20;                                           
SimParams.PayloadLength   = SimParams.NumberOfMessage * SimParams.MessageLength * 7; 
SimParams.FrameSize       = (SimParams.HeaderLength + SimParams.PayloadLength) ...
    / log2(SimParams.ModulationOrder);                                   
SimParams.FrameTime       = SimParams.Tsym*SimParams.FrameSize;

%% Tx parameters
SimParams.RolloffFactor     = 0.5;                                         
SimParams.ScramblerBase     = 2;
SimParams.ScramblerPolynomial           = [1 1 1 0 1];
SimParams.ScramblerInitialConditions    = [0 0 0 0];
SimParams.RaisedCosineFilterSpan = 10; % Filter span of Raised Cosine Tx Rx filters (in symbols)

%% Channel parameters
SimParams.PhaseOffset       = 47;   
SimParams.EbNo              = 13;   
SimParams.FrequencyOffset   = 5000; 
SimParams.DelayType         = 'Triangle'; 

%% Rx parameters
SimParams.DesiredPower                  = 2;            
SimParams.AveragingLength               = 50;           
SimParams.MaxPowerGain                  = 20;           
SimParams.MaximumFrequencyOffset        = 6e3;

K = 1;
A = 1/sqrt(2);
SimParams.PhaseRecoveryLoopBandwidth    = 0.01;         
SimParams.PhaseRecoveryDampingFactor    = 1;            
SimParams.TimingRecoveryLoopBandwidth   = 0.01;         
SimParams.TimingRecoveryDampingFactor   = 1;            

SimParams.TimingErrorDetectorGain       = 2.7*2*K*A^2+2.7*2*K*A^2; 
SimParams.PreambleDetectorThreshold     = 20;

%% Message generation and BER calculation parameters
msgSet = zeros(100 * SimParams.MessageLength, 1); 
for msgCnt = 0 : 99
    msgSet(msgCnt * SimParams.MessageLength + (1 : SimParams.MessageLength)) = ...
        sprintf('%s %03d\n', SimParams.Message, msgCnt);
end
SimParams.MessageBits = reshape(de2bi(msgSet, 7, 'left-msb')', [], 1);

SimParams.BerMask = zeros(SimParams.NumberOfMessage * length(SimParams.Message) * 7, 1);
for i = 1 : SimParams.NumberOfMessage
    SimParams.BerMask( (i-1) * length(SimParams.Message) * 7 + ( 1: length(SimParams.Message) * 7) ) = ...
        (i-1) * SimParams.MessageLength * 7 + (1: length(SimParams.Message) * 7);
end
